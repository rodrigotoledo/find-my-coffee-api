100.times do
  store = Store.create(
    name: Faker::Name.name,
    lonlat: "POINT(#{Faker::Address.longitude} #{Faker::Address.latitude})",
    address: Faker::Address.street_name,
    google_place_id: Faker::Address.building_number
    )
  15.times do
    store.ratings.build(value: rand(5), opinion: Faker::Lorem.paragraph, user_name: Faker::Name.first_name)
  end
  store.save
end 