class Store < ApplicationRecord
  has_many :ratings, autosave: true
  validates :lonlat, :name, :google_place_id, presence: true
  validates :google_place_id, uniqueness: true

  scope :within, -> (lon, lat, distance = 5) {
    where("ST_Distance(lonlat, 'POINT(? ?)') < ?", lon, lat, (distance * 1000))
  }

  def ratings_average
    return 0 if ratings.blank?
    (ratings.sum(:value) / ratings.count)
  end
end
