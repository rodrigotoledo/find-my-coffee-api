class Api::V1::StoresController < ApplicationController
  before_action :set_store, only: :show

  def index
    @stores ||= Store.within(params[:lon].to_f, params[:lat].to_f, 10000).sort_by(&:ratings_average).reverse
  end
  
  def show
  end
  
  protected
    def set_store
      @store ||= Store.where(google_place_id: params[:id]).first
    end
end
